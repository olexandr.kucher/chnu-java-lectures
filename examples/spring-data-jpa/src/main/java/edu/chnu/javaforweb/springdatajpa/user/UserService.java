package edu.chnu.javaforweb.springdatajpa.user;

import java.util.List;

public interface UserService {
    List<User> findAll();

    User save(User user);

    void deleteById(int userId);
}
