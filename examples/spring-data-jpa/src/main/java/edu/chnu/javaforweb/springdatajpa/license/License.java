package edu.chnu.javaforweb.springdatajpa.license;

import edu.chnu.javaforweb.springdatajpa.user.User;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;

@Entity
@Table(name = "license")
public class License implements Persistable<Integer> {
    private Integer id;
    private String name;
    private User user;

    public License() {}

    public License(String name) {
        this.name = name;
    }

    @Id
    @Override
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    @Transient
    public boolean isNew() {
        return null == getId();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "userId")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
