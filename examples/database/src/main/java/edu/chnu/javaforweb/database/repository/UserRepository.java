package edu.chnu.javaforweb.database.repository;

import edu.chnu.javaforweb.database.dto.User;

public interface UserRepository {
    User findBy(int id);

    User findByBPRM(int id);
}
