package edu.chnu.javaforweb.database.dto;

public record User(String firstName, String lastName) {
}
