package edu.chnu.javaforweb.database.repository;

import edu.chnu.javaforweb.database.dto.User;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
class UserRepositoryImpl implements UserRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    UserRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public User findBy(int id) {
        return jdbcTemplate.queryForObject(
                "SELECT firstName, lastName FROM users WHERE id = :userId",
                new MapSqlParameterSource("userId", id),
                (rs, rowNum) -> new User(
                        rs.getString("firstName"),
                        rs.getString("lastName")
                ));
    }

    @Override
    public User findByBPRM(int id) {
        return jdbcTemplate.queryForObject(
                "SELECT firstName, lastName FROM users WHERE id = :userId",
                new MapSqlParameterSource("userId", id),
                new BeanPropertyRowMapper<>(User.class)
        );
    }
}
