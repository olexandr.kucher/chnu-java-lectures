package edu.chnu.javaforweb.xmlconfig.logger;

public interface LoggerService {
    void print(Object object);
}
