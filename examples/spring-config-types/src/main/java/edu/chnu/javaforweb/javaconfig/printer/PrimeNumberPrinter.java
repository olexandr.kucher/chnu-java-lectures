package edu.chnu.javaforweb.javaconfig.printer;

public interface PrimeNumberPrinter {
    void printPrimeNumbers(int count);
}
