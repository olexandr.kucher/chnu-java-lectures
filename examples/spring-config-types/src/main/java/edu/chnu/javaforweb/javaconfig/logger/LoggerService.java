package edu.chnu.javaforweb.javaconfig.logger;

public interface LoggerService {
    void print(Object object);
}
