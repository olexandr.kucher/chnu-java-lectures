package edu.chnu.javaforweb.xmlconfig.logger;

@SuppressWarnings({"java:S106", "unused"})
public class LoggerServiceImpl implements LoggerService {
    private String loggerPrefix;

    public void setLoggerPrefix(String loggerPrefix) {
        this.loggerPrefix = loggerPrefix;
    }

    @Override
    public void print(Object object) {
        System.out.println("[" + loggerPrefix + "]: " + object);
    }

    public void init() {
        System.out.println("Initialized.");
    }

    private void destroy() {
        System.out.println("Destroyed.");
    }
}
