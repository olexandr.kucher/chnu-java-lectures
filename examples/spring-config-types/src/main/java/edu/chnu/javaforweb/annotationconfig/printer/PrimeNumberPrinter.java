package edu.chnu.javaforweb.annotationconfig.printer;

public interface PrimeNumberPrinter {
    void printPrimeNumbers(int count);
}
