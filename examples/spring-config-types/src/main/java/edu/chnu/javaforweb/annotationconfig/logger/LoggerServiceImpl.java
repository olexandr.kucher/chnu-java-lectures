package edu.chnu.javaforweb.annotationconfig.logger;

import edu.chnu.javaforweb.annotationconfig.LoggerPrefix;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Service
@SuppressWarnings("java:S106")
class LoggerServiceImpl implements LoggerService {
    private final String loggerPrefix;

    LoggerServiceImpl(@LoggerPrefix String loggerPrefix) {
        this.loggerPrefix = loggerPrefix;
    }

    @Override
    public void print(Object object) {
        System.out.println("[" + loggerPrefix + "]: " + object);
    }

    @PostConstruct
    public void init() {
        System.out.println("Initialized.");
    }

    @PreDestroy
    private void destroy() {
        System.out.println("Destroyed.");
    }
}
