package edu.chnu.javaforweb.xmlconfig;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
    public static void main(String[] args) {
        try( ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("edu/chnu/javaforweb/xmlconfig/context.xml")) {
            context.getBean(Worker.class).printPrimeNumbers(10);
        }
    }
}
