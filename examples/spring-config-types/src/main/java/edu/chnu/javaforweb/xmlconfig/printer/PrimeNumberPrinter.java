package edu.chnu.javaforweb.xmlconfig.printer;

public interface PrimeNumberPrinter {
    void printPrimeNumbers(int count);
}
