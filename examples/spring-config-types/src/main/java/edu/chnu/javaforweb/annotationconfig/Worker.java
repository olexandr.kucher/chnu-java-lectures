package edu.chnu.javaforweb.annotationconfig;

import edu.chnu.javaforweb.annotationconfig.printer.PrimeNumberPrinter;
import org.springframework.stereotype.Component;

@Component
class Worker {
    private final PrimeNumberPrinter primeNumberPrinter;

    Worker(PrimeNumberPrinter primeNumberPrinter) {
        this.primeNumberPrinter = primeNumberPrinter;
    }

    public void printPrimeNumbers(int number) {
        primeNumberPrinter.printPrimeNumbers(number);
    }
}
