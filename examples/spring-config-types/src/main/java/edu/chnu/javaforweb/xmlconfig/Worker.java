package edu.chnu.javaforweb.xmlconfig;

import edu.chnu.javaforweb.xmlconfig.printer.PrimeNumberPrinter;

public class Worker {
    private final PrimeNumberPrinter primeNumberPrinter;

    public Worker(PrimeNumberPrinter primeNumberPrinter) {
        this.primeNumberPrinter = primeNumberPrinter;
    }

    public void printPrimeNumbers(int number) {
        primeNumberPrinter.printPrimeNumbers(number);
    }
}
