package edu.chnu.javaforweb.annotationconfig.logger;

public interface LoggerService {
    void print(Object object);
}
