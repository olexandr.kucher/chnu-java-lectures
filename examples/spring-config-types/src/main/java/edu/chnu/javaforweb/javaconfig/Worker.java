package edu.chnu.javaforweb.javaconfig;

import edu.chnu.javaforweb.javaconfig.printer.PrimeNumberPrinter;

class Worker {
    private final PrimeNumberPrinter primeNumberPrinter;

    public Worker(PrimeNumberPrinter primeNumberPrinter) {
        this.primeNumberPrinter = primeNumberPrinter;
    }

    public void printPrimeNumbers(int number) {
        primeNumberPrinter.printPrimeNumbers(number);
    }
}
