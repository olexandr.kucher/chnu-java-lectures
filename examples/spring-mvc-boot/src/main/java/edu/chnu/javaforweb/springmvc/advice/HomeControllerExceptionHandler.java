package edu.chnu.javaforweb.springmvc.advice;

import edu.chnu.javaforweb.springmvc.controller.HomeController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import java.io.FileNotFoundException;

@SuppressWarnings("java:S106")
@ControllerAdvice(assignableTypes = HomeController.class)
public class HomeControllerExceptionHandler {
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({FileNotFoundException.class, RuntimeException.class})
    public ModelAndView exceptionHandler(Exception ex) {
        System.out.println("ERROR!!! " + ex.toString());
        return new ModelAndView("error").addObject("errorMessage", ex.toString());
    }
}