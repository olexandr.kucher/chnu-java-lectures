package edu.chnu.javaforweb.springsecurity.security.authprovider;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@ConditionalOnProperty(value = "spring.security.auth-impl", havingValue = "auth-provider")
class AuthenticationProviderSecurityConfig {
    private final AuthenticationProvider authenticationProvider;

    AuthenticationProviderSecurityConfig(AuthenticationProvider authenticationProvider) {
        this.authenticationProvider = authenticationProvider;
    }

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        return http
                .authorizeHttpRequests(customize -> customize
                        .antMatchers("/login").permitAll()
                        .antMatchers("/admin/**").hasAuthority("ROLE_ADMIN")
                        .antMatchers("/**").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN")
                )
                .formLogin(customize -> customize
                        .loginPage("/login")
                        .loginProcessingUrl("/auth")
                        .permitAll()
                        .usernameParameter("username")
                        .passwordParameter("password")
                )
                .logout(customize -> customize
                        .logoutUrl("/logout")
                        .logoutSuccessUrl("/login")
                )
                .authenticationProvider(authenticationProvider)
                .build();
    }
}
