package edu.chnu.javaforweb.springsecurity.security.userdetails;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@ConditionalOnProperty(value = "spring.security.auth-impl", havingValue = "user-details-service")
class UserDetailsServiceSecurityConfig {
    private final UserDetailsService userDetailsService;

    UserDetailsServiceSecurityConfig(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Bean
    PasswordEncoder passwordEncoder(@Value("${spring.security.secret-phrase}") String secret) {
        return new Pbkdf2PasswordEncoder(secret);
    }

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        return http
                .authorizeHttpRequests(customize -> customize
                        .antMatchers("/login").permitAll()
                        .antMatchers("/admin/**").hasAuthority("ROLE_ADMIN")
                        .antMatchers("/**").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN")
                )
                .formLogin(customize -> customize
                        .loginPage("/login")
                        .loginProcessingUrl("/auth")
                        .permitAll()
                        .usernameParameter("username")
                        .passwordParameter("password")
                )
                .logout(customize -> customize
                        .logoutUrl("/logout")
                        .logoutSuccessUrl("/login")
                )
                .userDetailsService(userDetailsService)
                .build();
    }
}
