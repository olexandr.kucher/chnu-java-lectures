package edu.chnu.javaforweb.springsecurity.security.authprovider;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

@Configuration
@ConditionalOnProperty(value = "spring.security.auth-impl", havingValue = "auth-provider")
class PasswordEncoderConfig {
    @Bean
    PasswordEncoder passwordEncoder(@Value("${spring.security.secret-phrase}") String secret) {
        return new Pbkdf2PasswordEncoder(secret);
    }
}
