package edu.chnu.javaforweb.springsecurity.user;

import java.util.Optional;

public interface UserService {
    Optional<User> findBy(int id);

    Optional<User> findBy(String username);
}
