package edu.chnu.javaforweb.springsecurity.user;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@SuppressWarnings({"java:S1192", "SqlDialectInspection"})
class UserRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    UserRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    Optional<User> findById(int id) {
        return jdbcTemplate.queryForStream(
                "SELECT firstName, lastName, username, password, role FROM application_user WHERE id = :userId",
                new MapSqlParameterSource("userId", id),
                (rs, rowNum) -> new User(
                        rs.getString("firstName"),
                        rs.getString("lastName"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("role")
                )
        ).findFirst();
    }

    Optional<User> findByUsername(String username) {
        return jdbcTemplate.queryForStream(
                "SELECT firstName, lastName, username, password, role FROM application_user WHERE username = :username",
                new MapSqlParameterSource("username", username),
                (rs, rowNum) -> new User(
                        rs.getString("firstName"),
                        rs.getString("lastName"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("role")
                )
        ).findFirst();
    }
}
