package edu.chnu.javaforweb.springsecurity.security.userdetailsinmemory;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(value = "spring.security.auth-impl", havingValue = "in-memory-user-details")
class InMemoryUserDetailsServiceImpl extends InMemoryUserDetailsManager {
    InMemoryUserDetailsServiceImpl() {
        super(
                User.withUsername("user").password("user").roles("USER").build(),
                User.withUsername("admin").password("admin").roles("ADMIN").build()
        );
    }
}
