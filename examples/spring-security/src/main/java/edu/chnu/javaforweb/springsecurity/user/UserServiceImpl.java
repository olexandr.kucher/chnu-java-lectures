package edu.chnu.javaforweb.springsecurity.user;

import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> findBy(int id) {
        return userRepository.findById(id);
    }

    @Override
    public Optional<User> findBy(String username) {
        return userRepository.findByUsername(username);
    }
}
