package edu.chnu.javaforweb.springsecurity.security.userdetailsinmemory;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.rememberme.InMemoryTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

@Configuration
@ConditionalOnProperty(value = "spring.security.auth-impl", havingValue = "in-memory-user-details")
class InMemoryUserDetailsServiceSecurityConfig {
    private final UserDetailsService userDetailsService;

    InMemoryUserDetailsServiceSecurityConfig(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Bean
    PersistentTokenRepository tokenRepository() {
        return new InMemoryTokenRepositoryImpl();
    }

    @Bean
    @SuppressWarnings({"deprecation", "java:S1874"})
    PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        return http
                .authorizeHttpRequests(customize -> customize
                        .antMatchers("/login").permitAll()
                        .antMatchers("/admin/**").hasAuthority("ROLE_ADMIN")
                        .antMatchers("/**").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN")
                )
                .formLogin(customize -> customize
                        .loginPage("/login")
                        .loginProcessingUrl("/auth")
                        .permitAll()
                        .usernameParameter("username")
                        .passwordParameter("password")
                )
                .logout(customize -> customize
                        .logoutUrl("/logout")
                        .logoutSuccessUrl("/login")
                )
                .rememberMe(customize -> customize
                        .useSecureCookie(true)
                        .rememberMeCookieName("RememberMe")
                        .key("RememberMeToken")
                        .tokenValiditySeconds(86400)
                        .tokenRepository(tokenRepository())

                )
                .userDetailsService(userDetailsService)
                .build();
    }
}
