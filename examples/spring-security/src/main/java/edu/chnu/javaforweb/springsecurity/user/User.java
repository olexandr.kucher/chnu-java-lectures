package edu.chnu.javaforweb.springsecurity.user;

public record User(
        String firstName,
        String lastName,
        String username,
        String password,
        String role
) {
}
