package edu.chnu.javaforweb.springsecurity.security.userdetails;

import edu.chnu.javaforweb.springsecurity.user.User;
import edu.chnu.javaforweb.springsecurity.user.UserService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@ConditionalOnProperty(value = "spring.security.auth-impl", havingValue = "user-details-service")
class UserDetailsServiceImpl implements UserDetailsService {
    private final UserService userService;

    UserDetailsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final Optional<User> optionalUser = userService.findBy(username);
        if (optionalUser.isPresent()) {
            final User user = optionalUser.get();
            return new org.springframework.security.core.userdetails.User(
                    user.username(),
                    user.password(),
                    List.of(new SimpleGrantedAuthority(user.role()))
            );
        } else {
            throw new UsernameNotFoundException("Incorrect login or password.");
        }
    }
}
