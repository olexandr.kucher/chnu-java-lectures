package edu.chnu.javaforweb.springsecurity.security.authprovider;

import edu.chnu.javaforweb.springsecurity.user.User;
import edu.chnu.javaforweb.springsecurity.user.UserService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@ConditionalOnProperty(value = "spring.security.auth-impl", havingValue = "auth-provider")
class AuthenticationProviderImpl implements AuthenticationProvider {
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    AuthenticationProviderImpl(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        final String username = authentication.getName();
        final String password = authentication.getCredentials().toString();
        final Optional<User> user = userService.findBy(username);
        if (user.isPresent() && passwordEncoder.matches(password, user.get().password())) {
            return new UsernamePasswordAuthenticationToken(
                    user.get(),
                    null,
                    List.of(new SimpleGrantedAuthority(user.get().role()))
            );
        } else {
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
