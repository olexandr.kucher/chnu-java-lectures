package edu.chnu.javaforweb.hibernate.user;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Repository
class UserRepository {
    private final SessionFactory sessionFactory;

    UserRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    Optional<User> findById(Integer id) {
        if (null == id) {
            return Optional.empty();
        } else {
            return Optional.ofNullable(sessionFactory.getCurrentSession().get(User.class, id));
        }
    }

    List<User> findAll() {
        final CriteriaBuilder builder = sessionFactory.getCurrentSession().getCriteriaBuilder();
        final CriteriaQuery<User> query = builder.createQuery(User.class);
        final Root<User> root = query.from(User.class);
        query.select(root);
        return sessionFactory
                .getCurrentSession()
                .createQuery(query)
                .getResultList();
    }

    User save(User user) {
        sessionFactory.getCurrentSession().save(user);
        return user;
    }

    void delete(User user) {
        sessionFactory.getCurrentSession().delete(user);
    }
}
