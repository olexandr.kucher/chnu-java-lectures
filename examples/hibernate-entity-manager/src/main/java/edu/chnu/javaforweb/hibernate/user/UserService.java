package edu.chnu.javaforweb.hibernate.user;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Optional<User> findById(int userId);

    Optional<User> findByUsername(String username);

    List<User> findAll();

    User save(User user);

    void deleteById(int userId);
}
