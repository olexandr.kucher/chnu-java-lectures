package edu.chnu.javaforweb.hibernate.license;

import edu.chnu.javaforweb.hibernate.config.GeneralRepository;
import edu.chnu.javaforweb.hibernate.config.GeneralRepositoryImpl;
import org.springframework.stereotype.Repository;

@Repository
class LicenseRepository extends GeneralRepositoryImpl<License, Integer> implements GeneralRepository<License, Integer> {
    LicenseRepository() {
        super(License.class);
    }
}
