package edu.chnu.javaforweb.hibernate.user;

import edu.chnu.javaforweb.hibernate.config.GeneralRepository;
import edu.chnu.javaforweb.hibernate.config.GeneralRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
class UserRepository extends GeneralRepositoryImpl<User, Integer> implements GeneralRepository<User, Integer> {
    UserRepository() {
        super(User.class);
    }

    Optional<User> findByUsername(String username) {
        return entityManager.createQuery("SELECT u FROM user u WHERE u.username = :username", User.class)
                .setParameter("username", username)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst();
    }
}
