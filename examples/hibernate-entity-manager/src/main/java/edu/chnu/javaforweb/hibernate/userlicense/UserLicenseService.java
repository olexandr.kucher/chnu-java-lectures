package edu.chnu.javaforweb.hibernate.userlicense;

import edu.chnu.javaforweb.hibernate.license.License;
import edu.chnu.javaforweb.hibernate.user.User;

import java.util.List;

public interface UserLicenseService {
    List<User> findAllUsers();

    List<License> findAllLicenses();

    void save(User user, License license);

    void deleteUserById(Integer id);
}
