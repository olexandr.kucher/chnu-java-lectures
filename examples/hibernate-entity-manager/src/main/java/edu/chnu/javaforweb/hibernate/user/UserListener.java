package edu.chnu.javaforweb.hibernate.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;

public class UserListener {
    private final Logger logger = LoggerFactory.getLogger(UserListener.class);

    @PrePersist
    public void prePersist(User user) {
        logger.info("username: {}, id: {}", user.getUsername(), user.getId());
    }

    @PostPersist
    public void postPersist(User user) {
        logger.info("username: {}, id: {}", user.getUsername(), user.getId());
    }
}
