package edu.chnu.javaforweb.springdatajdbc.userlicense;

import edu.chnu.javaforweb.springdatajdbc.license.License;
import edu.chnu.javaforweb.springdatajdbc.user.User;

import java.util.List;
import java.util.Map;

public interface UserLicenseService {
    List<User> findAllUsers();

    Map<Integer, List<License>> findAllLicensesForUsers();

    void createUserWithLicense(String username, String firstName, String lastName);

    void deleteByUserId(int userId);
}
