package edu.chnu.javaforweb.springmvc.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.logging.Logger;

@SuppressWarnings({"NullableProblems", "java:S2629"})
public class ControllerLogInterceptor implements HandlerInterceptor {
    private final Logger logger = Logger.getLogger(getClass().getSimpleName());

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        logger.info("[" + LocalDateTime.now() + "]: START:    " + request.getMethod() + " " + request.getRequestURI());
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        if (ex != null) {
            logger.info("[" + LocalDateTime.now() + "]: ERROR:    " + request.getMethod() + " " + request.getRequestURI() + ": " + ex);
            response.sendRedirect("error.html");
        } else {
            logger.info("[" + LocalDateTime.now() + "]: COMPLETE: " + request.getMethod() + " " + request.getRequestURI());
        }
    }
}
