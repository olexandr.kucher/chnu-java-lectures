package edu.chnu.javaforweb.dependencyinjection.application.logger;

import edu.chnu.javaforweb.dependencyinjection.dependencyinjector.annotations.Bean;
import edu.chnu.javaforweb.dependencyinjection.dependencyinjector.annotations.Destroy;
import edu.chnu.javaforweb.dependencyinjection.dependencyinjector.annotations.Init;

@Bean
@SuppressWarnings("java:S106")
public class LoggerServiceImpl implements LoggerService {
    @Override
    public void print(Object object) {
        System.out.println(object);
    }

    @Init
    public void init() {
        System.out.println("Initialized.");
    }

    @Destroy
    private void destroy() {
        System.out.println("Destroyed.");
    }
}
