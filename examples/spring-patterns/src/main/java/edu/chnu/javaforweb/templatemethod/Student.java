package edu.chnu.javaforweb.templatemethod;

import edu.chnu.javaforweb.templatemethod.templates.PassExamTemplate;

public class Student {
    private final PassExamTemplate template;

    public Student(PassExamTemplate template) {
        this.template = template;
    }

    public PassExamTemplate getTemplate() {
        return template;
    }
}
