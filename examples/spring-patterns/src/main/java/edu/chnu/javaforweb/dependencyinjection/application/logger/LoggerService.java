package edu.chnu.javaforweb.dependencyinjection.application.logger;

public interface LoggerService {
    void print(Object object);
}
