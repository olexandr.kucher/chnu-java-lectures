package edu.chnu.javaforweb.strategy;

import edu.chnu.javaforweb.Exam;
import edu.chnu.javaforweb.strategy.strategies.BadStudentPassExamStrategy;
import edu.chnu.javaforweb.strategy.strategies.ExcellentStudentPassExamStrategy;
import edu.chnu.javaforweb.strategy.strategies.RegularStudentPassExamStrategy;

import java.time.LocalDateTime;
import java.time.Month;

public class PassExamMain {
    public static void main(String[] args) {
        final Exam exam = new Exam();
        exam.setName("Programing Languages");
        exam.setTeacher("FirstName LastName");
        exam.setTime(LocalDateTime.of(2018, Month.MAY, 25, 9, 0));

        passExam(new Student(new ExcellentStudentPassExamStrategy()), exam);
        passExam(new Student(new BadStudentPassExamStrategy()), exam);
        passExam(new Student(new RegularStudentPassExamStrategy()), exam);
    }

    private static void passExam(Student student, Exam exam) {
        student.getStrategy().passExam(exam);
    }
}
