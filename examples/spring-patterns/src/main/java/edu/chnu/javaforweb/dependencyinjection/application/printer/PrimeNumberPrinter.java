package edu.chnu.javaforweb.dependencyinjection.application.printer;

public interface PrimeNumberPrinter {
    void printPrimeNumbers(int count);
}
