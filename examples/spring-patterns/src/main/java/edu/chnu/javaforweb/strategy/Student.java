package edu.chnu.javaforweb.strategy;

import edu.chnu.javaforweb.strategy.strategies.PassExamStrategy;

public class Student {
    private final PassExamStrategy strategy;

    public Student(PassExamStrategy strategy) {
        this.strategy = strategy;
    }

    public PassExamStrategy getStrategy() {
        return strategy;
    }
}
