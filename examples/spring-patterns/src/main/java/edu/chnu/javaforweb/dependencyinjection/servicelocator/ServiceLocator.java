package edu.chnu.javaforweb.dependencyinjection.servicelocator;

import java.util.Collection;

public interface ServiceLocator {
    void lookupForBeans(String basePackage);

    <T> T getBean(Class<T> beanType);

    Collection<Object> getAllBeans();
}
