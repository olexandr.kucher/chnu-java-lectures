package edu.chnu.javaforweb.dependencyinjection.dependencyinjector;

public interface DependencyInjector extends AutoCloseable {
    <T> T getBean(Class<T> beanType);
}
