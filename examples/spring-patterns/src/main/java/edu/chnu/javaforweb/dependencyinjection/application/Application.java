package edu.chnu.javaforweb.dependencyinjection.application;

import edu.chnu.javaforweb.dependencyinjection.dependencyinjector.DependencyInjector;
import edu.chnu.javaforweb.dependencyinjection.dependencyinjector.DependencyInjectorImpl;

public class Application {
    public static void main(String[] args) throws Exception {
        try (DependencyInjector context = new DependencyInjectorImpl(Application.class)) {
            context.getBean(Worker.class).printPrimeNumbers(10);
        }
    }
}
