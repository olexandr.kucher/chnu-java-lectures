package edu.chnu.javaforweb.strategy.strategies;

import edu.chnu.javaforweb.Exam;

public interface PassExamStrategy {
    void passExam(Exam exam);
}
