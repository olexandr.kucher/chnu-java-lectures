package edu.chnu.javaforweb.dependencyinjection.application;

import edu.chnu.javaforweb.dependencyinjection.application.printer.PrimeNumberPrinter;
import edu.chnu.javaforweb.dependencyinjection.dependencyinjector.annotations.Bean;

import javax.inject.Inject;

@Bean
public class Worker {
    @Inject
    private PrimeNumberPrinter primeNumberPrinter;

    public void printPrimeNumbers(int number) {
        primeNumberPrinter.printPrimeNumbers(number);
    }
}
