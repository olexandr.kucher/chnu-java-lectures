# ChNU Java Lectures

Java for Web project for ChNU

#######################################################

| Lesson |       Date        | Topic                              |
|:------:|:-----------------:|:-----------------------------------|
|   1    | February 23, 2023 | Intro                              |
|   2    |   March 2, 2023   | Built-In Classes                   |
|   3    |   March 9, 2023   | Object Oriented Programming        |
|   4    |  March 16, 2023   | Generics. Collections Framework    |
|   5    |  March 23, 2023   | Code Testing (Unit Testing)        |
|   6    |  March 30, 2023   | Functional Programming, Stream API |
|   7    |   April 6, 2023   | Exceptions, Input/Output           |
|   8    |  April 13, 2023   | Reflection API, Annotations        |
|   9    |  April 20, 2023   | Servlet API (Web)                  |
|   10   |  April 27, 2023   | Spring (IoC, MVC, Boot)            |
|   11   |    May 4, 2023    | JDBC, Spring JDBC, Flyway          |
|   12   |   May 11, 2023    | Spring Security                    |
|   13   |   May 18, 2023    | REST, Swagger                      |
|   14   |   May 25, 2023    | Gradle, Integration Testing        |
|   15   |   June 1, 2023    | Concurrency                        |
|   16   |   June 8, 2023    | ORM: Hibernate, Spring Data        |