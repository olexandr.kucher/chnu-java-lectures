package edu.chnu.javaforweb.coders;

public interface Decoder {

    String decode(String input);
}
