package edu.chnu.javaforweb.coders;

public enum Algorithm {
    CAESAR,
    MORSE;
}
