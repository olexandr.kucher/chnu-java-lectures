package edu.chnu.javaforweb.coders;

public interface Encoder {

    String encode(String input);
}
