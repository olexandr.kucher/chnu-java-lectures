package edu.chnu.javaforweb.util;

public class NotImplementedException extends RuntimeException {
    public NotImplementedException() {
        super("Implement me, please!");
    }
}
