package edu.chnu.javaforweb.coders;

import edu.chnu.javaforweb.util.NotImplementedException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

class EncodersFactoryTest {

    @Test
    void getEncoder_should_throwNotImplementedException_when_getCaesarEncoder() {
        assertThrowsExactly(NotImplementedException.class, () -> EncodersFactory.getEncoder("CAESAR"));
    }
}